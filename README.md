# README #

Dimando - Frontend assessment
November 2017
By Ilaria Brizi

//JS libraries
//jquery.min.js jQuery v3.2.1
//html5shiv.js HTML5 Shiv 3.7.3 
//isotope-docs.min.js

//CSS package
//normalize.css normalize.css v7.0.0
//grid.css Bootstrap Grid v4.0.0

//Fonts
//CDN Open Sans font


NOTES:
Because it is a repository, I included files compiled directly. I only used CDN for fonts. 
CSS files are compiled but not minified to stay more readable.
I reduced the dimensions of provided images, to make them smaller and lighter.
I added a footer, it wasn't required, but I felt it missing.